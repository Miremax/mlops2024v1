FROM python:3.11-slim

RUN pip install poetry

WORKDIR /Projects
COPY . .
RUN poetry config virtualenvs.in-project true
RUN poetry install
ENTRYPOINT [ "poetry","run", "python","example.py" ]