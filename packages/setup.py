from setuptools import find_packages, setup

setup(
    name="greeting",
    author="Miremax",
    version="0.1",
    description="Library for Greetings!",
    packages=find_packages(),
    include_package_data=True,
    classifiers=["Python 3.9", "GitLab"],
    python_requires=">=3.6",
    setup_requires=["setuptools-git-versioning"],
)
